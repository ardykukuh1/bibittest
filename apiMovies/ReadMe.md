# Guide to Run the API Movie
This repository contains the TESTS for BackEnd NodeJS

## Setup the project on cd /apiMovies 
1. npm install
2. setup mysql server on your own db server || DBName: 'bibit_development' 
3. setup database connection in config/config.json with your database privilage
4. run migration database : npx sequalize db:migrate
5. move/copy .env-example to .env
6. update value for API_KEY with faf7e5bb 

### Run mode 
1. npm run dev 
2. npm run test 
 
* for search `http://localhost:8080/v1/movie/search?s={containword}`
* for detail `http://localhost:8080/v1/movie/detail?movieId={movieId}`
 