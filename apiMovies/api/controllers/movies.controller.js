const ParseApi = require('../helper/parse')
const ResponseApi = require('../helper/response') 
const {baseAPIMovies} = require('../../config/constant')   
const DataLog = require('../../models').datalog
  
const moviesController = () => { 
    const addLog = async (data) => {
        const createLog = await DataLog.create(data) 
        return createLog
    }
    const search = async (req, res, next) => {
        req.checkQuery('s', 'Query string s required').notEmpty()
        const error = req.validationErrors()
        if (error) {
            return ResponseApi.errorResult(res, error)
        }
    
        const { s, page } = req.query 
        const url = `${baseAPIMovies}?apikey=${process.env.API_KEY}&s=${s}&page=${page}`
    
        try {
            const result = await ParseApi.get(url)
    
            if (result.statusCode && result.statusCode !== 200) {
                return ResponseApi.errorResult(res, JSON.parse(result.body), result.statusCode)
            }
            const endpoint = `${req.baseUrl}${req.path}` 

            // add log
            await addLog({
                host: baseAPIMovies,
                endpoint: endpoint,
                search: s,
                page: page
            })
    
            return ResponseApi.dataResult(res, result)
        } catch (error) {
            return ResponseApi.errorResult(res, error)
        }
    }

    const detail = async (req, res, next) => {
        req.checkQuery('movieId', 'Query MovieId is required').notEmpty()
        const error = req.validationErrors()
        if (error) {
            return ResponseApi.errorResult(res, error)
        }
      
        const { movieId } = req.query 
        const url = `${baseAPIMovies}?apikey=${process.env.API_KEY}&i=${movieId}`
      
        try {
            const result = await ParseApi.get(url)
      
            if (result.statusCode && result.statusCode !== 200) {
                return ResponseApi.errorResult(res, JSON.parse(result.body), result.statusCode)
            }
      
            const endpoint = `${req.baseUrl}${req.path}` 
      
            // add log
            await addLog({
                host: baseAPIMovies,
                endpoint: endpoint,
                movieId: movieId
            })
      
            return ResponseApi.dataResult(res, result)
        } catch (error) {
            return ResponseApi.errorResult(res, error)
        }
    }
    return {
        addLog,
        search,
        detail
    }
}

  
module.exports = moviesController;