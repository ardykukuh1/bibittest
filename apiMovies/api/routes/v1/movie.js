let Route = express.Router()
const controllers = require('../../controllers'); 
const moviesController = controllers.moviesController()
Route.get('/search', moviesController.search)
     .get('/detail', moviesController.detail)


module.exports = Route