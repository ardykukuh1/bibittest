'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class datalog extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  datalog.init({
    host: DataTypes.STRING,
    endpoint: DataTypes.STRING,
    search: DataTypes.STRING,
    page: DataTypes.INTEGER,
    movieId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'datalog',
  });
  return datalog;
};