/* ======================================================
3. Please refactor the code below to make it more concise, efficient and readable with good logic flow. 
function findFirstStringInBracket(str){
   if(str.length > 0){
   	  let indexFirstBracketFound = str.indexOf("(");
   	  if(indexFirstBracketFound >= 0){
	      let wordsAfterFirstBracket = str.substr( indexFirstBracketFound );
	      if(wordsAfterFirstBracket){
	         wordsAfterFirstBracket = wordsAfterFirstBracket.substr(1);
	         let indexClosingBracketFound = wordsAfterFirstBracket.indexOf(")");
	         if(indexClosingBracketFound >= 0){
	         	return wordsAfterFirstBracket.substring(0, indexClosingBracketFound);
	         }
	         else{
	         	return '';
	         }
	      }else{
	         return '';
	      }
      }else{
      	return '';
      }
   }else{
      return '';
   }
} 
=========================================================*/

function findFirstStringInBracket(str){
    if(!str) return '';
    let indexFirstBracketFound = str.indexOf("("); 
    let wordsAfterFirstBracket = str.substr( indexFirstBracketFound+1 );  
    if(indexFirstBracketFound >= 0 && wordsAfterFirstBracket){ 
        let indexClosingBracketFound = wordsAfterFirstBracket.indexOf(")"); 
        return (indexClosingBracketFound >= 0) ? wordsAfterFirstBracket.substring(0, indexClosingBracketFound) : ''; 
    }else{
        return '';
    } 
}

