/*===================================================================
4. Logic Test
Anagram adalah istilah dimana suatu string jika dibolak balik ordernya maka akan sama eg. 'aku' dan 'kua' adalah Anagram, 'aku' dan 'aka' bukan Anagram.
Dibawah ini ada array berisi sederetan Strings.
['kita', 'atik', 'tika', 'aku', 'kia', 'makan', 'kua']
Silahkan kelompokkan/group kata-kata di dalamnya sesuai dengan kelompok Anagramnya,
Catatan: tidak boleh menggunakan syntax es6 map, reduce, find, filter
# Expected Outputs
[
   ["kita", "atik", "tika"],
   ["aku", "kua"],
   ["makan"],
   ["kia"]
]
======================================================================*/

var words = ['kita', 'atik', 'tika', 'aku', 'kia', 'makan', 'kua'];
let output = []
for ( var i = 0; i < words.length; i++) {
    if(words[i] == '') continue
    let tmp = []
        tmp.push(words[i])

    var word = words[i];
    var alphabetical = word.split("").sort().join("");

    for (var j = 0; j < words.length; j++) {

        if (i === j) {
            continue;
        }

        var other = words[j];
        if(alphabetical === other.split("").sort().join("")){
            console.log(word + " - " + other + " (" + i + ", " + j + ")");
            tmp.push(words[j])
                    words[j] = ''
        }
    }
    output.push(tmp)
}
console.log(output)